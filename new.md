# 项目课


## 项目介绍

### Web全栈开发: 微博客

+ 项目名称: vblog, 一套前后端分离的单体Web服务
+ 适合对象: 新手项目入门
+ 业务方向: Web全栈开发, 运维开发
+ 难度: 中
+ 教学方式: 全程手写代码
+ 项目关键技术:
    1. 使用Gin框架开发Restful API
    2. 使用Gorm来与MySQL进行交互
    3. OOP思想在项目的运用
    4. 面向接口与面向组合思想在项目中的应用
    5. 使用TDD方式开发业务模块
    6. 使用ioc解决对象依赖问题
    7. 业务Debug技巧
    8. 自定义业务异常
    9. API返回数据的统一处理
    10. 面向业务分区的统一架构
    11. 基于Token的简化版 RBAC认证
    12. 配置文件同时支持Env和配置文件
    13. Web基础与前端布局技术从0到1实战
    14. 前端框架采用Vue3开发
    15. UI组件库采用 ArcoDesign
    16. Restful API客户端 SDK开发
    
#### 架构设计

![](./image/projects/vblog-arch.png)


#### UI展示

![](./image/projects/vblog_login.png)

![](./image/projects/frontend-blog-list.png)

![](./image/projects/backend-blog-list.png)

![](./image/projects/blog-edit.png)


### 微服务开发: 微服务研发云迷你版

+ 项目名称: devcloud-mini
+ 适合对象: 有基本的单体项目开发经验，想深入进阶微服务开发的同学
+ 业务方向: 大型分布式业务系统研发, 业务中台, 比较低层服务(上层的业务是根据市场变化), 业务中间件(网关)
+ 难度: 中高
+ 教学方式: 后端代码手写 + 前端代码提供样例
+ 项目关键技术:
    1. 使用Go Workspace 进行微服务多模块开发
    2. 外部调用接口 采用goRestful 开发
    3. 内部调用接口 采用GRPC 开发
    4. 采用kafka进行异步消息通讯
    5. 链路追踪: opentelemetry 接入Trace
    6. 应用自定义监控: 使用prometheus SDK开发Exporter
    7. 如何使用ioc托管配置对象
    8. GRPC客户端开发
    9. Restful API 和 Grpc API中间件开发
    10. 使用Makefile作为项目脚手架


#### 微服务用户与权限中心

##### 架构设计

![](./image/mcenter/mcenter_auth.png)

![](./image/mcenter/mcenter_perm.png)

##### UI展示

+ 登录页面

![](./image/projects/devcloud-login.png)

+ 管理后台

![](./image/projects/devcloud-ucenter-user.png)
![](./image/projects/devcloud-ucenter-role.png)




#### 微服务审计

##### 架构设计

![](./image/maudit/maudit.png)

##### UI展示

![](./image/projects/devcloud-ucenter-audit.png)

#### CMDB

##### 架构设计

![](./image/cmdb/cmdb.png)


##### UI展示

![](./image/cmdb/cmdb-ui.png)


### 复杂业务开发解读: 微服务研发云平台: 流水线项目

+ 项目名称: devcloud
+ 适合对象: 熟悉微服务开发模式, 了解复杂业务开发流程, 想要从事平台开发业务方向的同学
+ 业务方向: 平台开发
+ 难度: 高
+ 教学方式: 代码解读与演示


#### 发布中心

基于k8s的微服务发布平台, 基础功能:
+ 多个Kubernetes集群管理
+ 服务的部署与创建
+ 服务日志查看
+ 服务控制台登录

##### 架构设计


##### UI展示

![](./image/projects/devcloud-mpaas-service.png)
![](./image/projects/devcloud-mpaas-deploy.png)
![](./image/projects/devcloud-mpaas-log.png)
![](./image/projects/devcloud-mpaas-console.png)


#### 流水线编排

基于k8s job设计的 流水线服务, 基础功能:
+ Job的管理
+ Job执行与调试
+ Pipeline的管理
+ Pipeline的执行与调试
+ Pipeline 触发器管理(与Gitlab联动)

##### 架构设计

![](./image/mflow/mflow.png)

##### UI展示

![](./image/projects/devcloud-mflow-job-edit.png)
![](./image/projects/devcloud-mflow-job-run.png)
![](./image/projects/devcloud-mflow-pipeline.png)
![](./image/projects/devcloud-mflow-pipeline-job.png)

## 课程大纲

### Web全栈开发

+ Vblog项目介绍与骨架搭建
    1. 项目概要设计: 需求,流程,原型与架构
    2. 项目详细设计: 接口,数据库
    3. 业务代码组织流程设计
    4. 项目架构风格: 功能分区与业务分区 

+ Vblog项目V1简化版开发: 用户管理模块开发
    1. 用户管理模块的接口定义
    2. 如何管理项目的配置
    3. 用户管理模块接口实现与测试
    4. 基于Bcrypt处理用户密码存储问题

+ Vblog项目V1简化版开发: 登录管理模块开发
    1. 认证流程设计与令牌管理服务的接口定义
    2. 令牌管理服务的服务实现与测试
    3. 利用Gin框架实现Restful风格的登录接口开发
    4. 通过main组装好程序并使用Postman进行接口测试

+ Vblog基于Ioc优化程序依赖管理
    1. 对象依赖管理与Ioc思想
    2. 实现一个简单的ioc库
    3. 基于Ioc重构控制器与API Handler
    4. 为程序添加CLI

+ Vblog基于Ioc开发博客管理模块
    1. 博客管理模块的接口定义与实现
    2. 为博客管理模块添加Restful接口
    3. API认证中间件开发与使用
    4. 工程优化: 优雅关闭与Makefile脚手架

+ Web入门
    1. JavaScript基础
    2. HTML基础入门
    3. CSS基础入门
    4. 浏览器基础

+ Vue入门
    1. Vue初体验
    2. 前端发展史与MVVM思想的诞生
    3. Vue实例与生命周期
    4. Vue响应式原理与模版语法

+ Vue进阶与Vblog前端框架
    1. Vue组件基础
    2. VueUI组件库与Arco Design的基本使用
    3. Vblog工程初始化
    4. 前端Layout布局

+ Vlog前端开发
    1. 登录页面开发
    2. 博客前台与后台切换
    3. 页面访问保护(导航守卫)
    4. 后台博客管理页面开发

### 微服务开发之Devcloud

+ 微服务基础之 RPC与Protobuf基础
    1. Go语音内置RPC框架的使用
    2. 基于接口封装优化好的RPC
    3. Protobuf介绍与环境准备
    4. Protobuf编解码与语法介绍

+ 微服务基础之 GRPC与Ioc融入
    1. GRPC入门之客户端与服务端
    2. GRPC中间件与认证
    3. 基于Grpc开发Vblog评论模块
    4. vblog评论模块GRPC控制器托管Ioc

+ 微服务Devcloud研发平台开发: 用户中心-中心化认证
    1. DevCloud需求,功能与架构设计
    2. 微服务多工程项目组织方式介绍
    3. 微服务通用认证流程设计
    4. 用户中心 认证服务端与客户端开发
    5. 用户中心 认证介入中间件开发

+ 微服务Devcloud研发平台开发: 用户中心-中心化鉴权
    1. 其他服务 使用认证中间件接入用户中心
    2. 微服务通用RBAC鉴权流程设计
    3. 用户中心 权限服务端与客户端开发
    4. 认证中间件补充鉴权逻辑

+ 微服务Devcloud研发平台开发: CMDB设计与凭证管理
    1. 常见的CMDB设计模式
    2. 类云管CMDB设计方案与流程
    3. 资源管理模块开发 
    4. 云商凭证管理模块开发

+ 微服务Devcloud研发平台开发: CMDB资源同步与审计中心
    1. 开发腾讯云VM资源同步Provider
    2. Secret模块集合Provider实现云资源同步
    3. 消息队列与Kafka
    4. 审计中心基于消息队列模式的设计与实现

+ 微服务工程基础: 应用自定义监控与链路追踪
    1. Prometheus 概念介绍和Exporter 开发基础
    2. 审计中心 基于Exporter模式 实现自定义监控
    3. 基于OpenTelemery链路追踪技术介绍
    4. cmdb接入Trace 实战

### 基于K8s Operator的 CI CD项目解读

+ 自研CICD平台代码解读: mpaas与mflow
    1. Kubernetes 简介与client-go使用
    2. 基于kubeconf设计多集群管理系统
    3. 使用k8s job的流水线设计方案解读
    4. Docker Build Job执行演示

+ 自研CICD平台代码解读: k8s operator开发
    1. Pipeline流程代码解读
    2. Gitlab触发流程代码解读
    3. k8s operator 开发模式解读